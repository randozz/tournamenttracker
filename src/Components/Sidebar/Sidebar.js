import React, { useContext, useState } from "react";
import { Card, Collapse, Paper, Tooltip } from "@mui/material";
import { Link, useNavigate } from "react-router-dom";
import AddCircleOutlineTwoToneIcon from "@mui/icons-material/AddCircleOutlineTwoTone";
import SettingsApplicationsTwoToneIcon from "@mui/icons-material/SettingsApplicationsTwoTone";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ExpandMore from "@mui/icons-material/ExpandMore";
import { ExpandMoreOutlined } from "@mui/icons-material";
import HomeTwoToneIcon from "@mui/icons-material/HomeTwoTone";

import { UserContext } from "../../UserContext";
import metrixDb from "../../Server/db.json";
import { Box } from "@mui/system";

export const Sidebar = ({ callback }) => {
	const { setShowCompModal } = useContext(UserContext);
	const [expanded, setExpanded] = useState({});
	const navigate = useNavigate();

	const handleExpandClick = (id) => {
		setExpanded((prevState) => ({ ...prevState, [id]: !prevState[id] }));
	};

	return (
		<Box
			sx={{
				backgroundColor: "#F8F8FF",
				width: "20vw",
				minWidth: "200px"
			}}
		>
			<Card
				elevation={3}
				sx={{
					width: "auto",
					padding: "10px",
					backgroundColor: "#00C9A7",
					borderBottom: "1px solid #FBFBFF"
				}}
			>
				<Tooltip title="Lisää kisa" placement="bottom">
					<AddCircleOutlineTwoToneIcon
						sx={{
							fontSize: 40,
							color: "#FBFBFF",
							"&:hover": {
								color: "black"
							}
						}}
						onClick={() => setShowCompModal(true)}
					/>
				</Tooltip>
				<Tooltip title="Koti" placement="bottom">
					<HomeTwoToneIcon
						sx={{
							fontSize: 40,
							color: "#FBFBFF",
							float: "right",
							"&:hover": {
								color: "black"
							}
						}}
						onClick={() => navigate("/")}
					/>
				</Tooltip>
				<Tooltip title="Asetukset" placement="bottom">
					<SettingsApplicationsTwoToneIcon
						sx={{
							fontSize: 40,
							color: "#FBFBFF",
							float: "right",
							"&:hover": {
								color: "black"
							}
						}}
					/>
				</Tooltip>
			</Card>
			<div
				style={{
					width: "auto",
					marginTop: "5px",
					paddingBottom: "5px",
					paddingTop: "5px",
					display: "inherit",
					height: "auto",
					backgroundColor: "#F8F8FF"
				}}
			>
				{metrixDb.Competition.filter(
					(value, index, self) =>
						index ===
						self.findIndex(
							(event) =>
								event.tournamentName === value.tournamentName
						)
				).map((tournament) => {
					const saveName = tournament.tournamentName;

					return (
						<Card
							style={{
								fontFamily: "Helvetica",
								width: "-webkit-fill-available",
								height: "fit-content",
								backgroundColor: "#F8F8FF",
								display: "inline-block",
								paddingBottom: "5px"
							}}
							key={tournament.tournamentName}
						>
							<Paper
								elevation={1}
								sx={{
									padding: "10px",
									marginTop: "0.3px",
									border: "px solid black",
									backgroundColor: "#2C8779",
									borderRadius: "0px"
								}}
							>
								<Link
									to={`/tournament/${tournament.id}`} // todo better link than id number
									key={tournament.id}
									sx={{ color: "#FBFBFF" }}
									underline="hover"
									onClick={() => callback(tournament)}
								>
									{tournament.tournamentName}
								</Link>

								<ExpandMore
									onClick={() =>
										handleExpandClick(
											tournament.tournamentName
										)
									}
									aria-expanded={ExpandMoreOutlined}
									aria-label="show more"
									sx={{ float: "right" }}
								>
									<ExpandMoreIcon />
								</ExpandMore>
							</Paper>

							<Collapse
								in={expanded[tournament.tournamentName]}
								timeout="auto"
								unmountOnExit
								title={tournament.tournamentName}
							>
								{metrixDb.Competition.map((event) => {
									if (event.tournamentName === saveName)
										return (
											<Paper
												elevation={1}
												key={event.metrixId}
												sx={{
													fontFamily: "Helvetica",
													paddingTop: "5px",
													paddingLeft: "10px",
													display: "block",
													width: "auto",
													height: "25px",
													marginLeft: "15px",
													borderBottom:
														"1px black solid",
													borderLeft:
														"1px black solid",
													borderRight:
														"1px black solid",
													borderRadius: "3px",
													backgroundColor: "#D0EBE5"
												}}
											>
												<Link
													to={`/competition/${event.metrixId}`} // todo better link than id number
													key={event.metrixId}
													sx={{ color: "#2F141A" }}
													underline="hover"
													onClick={() =>
														callback(event)
													}
												>
													{event.competitionName}
												</Link>
											</Paper>
										);
								})}
							</Collapse>
						</Card>
					);
				})}
			</div>
		</Box>
	);
};
