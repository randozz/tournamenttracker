import { Box, Paper } from "@mui/material";
import React from "react";
import { PlayerList } from "./PlayerList";

export const CompetitionInfo = ({ data }) => (
	<Box
		sx={{
			width: 1,
			minHeight: "fit-content",
			border: "10px #00C9A7 solid",
			backgroundColor: "#00C9A7",
			top: "10px",
			borderRadius: "10px",
			display: "grid",
			gridTemplateColumns: "repeat(6, 1fr)",
			gridTemplateRows: "1fr",
			gridColumnGap: "5px",
			gridRowGap: "0px"
		}}
	>
		<Paper
			elevation={2}
			sx={{
				gridArea: "1 / 1 / 2 / 3",
				width: "-webkit-fill-available",
				minHeight: "-webkit-fill-available",
				padding: "10px",
				backgroundColor: "#F8F8FF"
			}}
		>
			{data.competitionName}
			<br />
			{data.Date} (klo {data.Time})
			<hr />
			{data.CourseName}
			<br />
			{data.metrixId}
			<br />
			{data.Comment}
		</Paper>
		<Paper
			elevation={2}
			sx={{
				gridArea: "1 / 3 / 2 / 7",
				width: "-webkit-fill-available",
				minHeight: "-webkit-fill-available",
				padding: "10px",
				backgroundColor: "#F8F8FF"
			}}
		>
			<PlayerList metrixData={data} />
		</Paper>
	</Box>
);
