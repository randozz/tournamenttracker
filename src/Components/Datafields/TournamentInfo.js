import { Paper } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";

export const TournamentInfo = ({ data }) => (
	<Box
		sx={{
			border: "10px #00C9A7 solid",
			backgroundColor: "#00C9A7",
			borderRadius: "10px",
			display: "grid",
			gridTemplateColumns: "repeat(4, 1fr)",
			gridTemplateRows: "repeat(4, 1fr)",
			gridColumnGap: "5px",
			gridRowGap: "5px",
			height: "95vh",
			minHeight: "fit-content",
			width: 1
		}}
	>
		<Paper
			elevation={2}
			sx={{
				gridArea: "1 / 1 / 3 / 3",
				padding: "10px",
				backgroundColor: "#F8F8FF"
			}}
		>
			{data.tournamentName}
			<hr />
		</Paper>
		<Paper
			elevation={2}
			sx={{
				gridArea: "3 / 1 / 5 / 3",
				padding: "10px",
				backgroundColor: "#F8F8FF"
			}}
		>
			2
			<hr />
		</Paper>
		<Paper
			elevation={3}
			sx={{
				gridArea: "1 / 3 / 5 / 5",
				padding: "10px",
				backgroundColor: "#F8F8FF"
			}}
		>
			<hr />
		</Paper>
	</Box>
);
