import React, { useEffect, useRef, useState } from "react";
import {
	Button,
	ButtonGroup,
	ClickAwayListener,
	Collapse,
	Grow,
	IconButton,
	MenuItem,
	MenuList,
	Paper,
	Popper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow
} from "@mui/material";
import { Box } from "@mui/system";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import { useLocation } from "react-router-dom";

const Row = ({ row, tracks }) => {
	const [open, setOpen] = useState(false);

	return (
		<>
			<TableRow sx={{ "& > *": { borderBottom: "unset" } }}>
				<TableCell align="right">{row.OrderNumber}</TableCell>
				<TableCell component="th" scope="row">
					{row.Name}
				</TableCell>
				<TableCell align="right">{row.Diff}</TableCell>
				<TableCell align="right">{row.Sum}</TableCell>
				<TableCell align="right">{row.Position}</TableCell>

				<TableCell>
					<IconButton
						aria-label="expand row"
						size="small"
						onClick={() => setOpen(!open)}
					>
						{open ? (
							<KeyboardArrowUpIcon />
						) : (
							<KeyboardArrowDownIcon />
						)}
					</IconButton>
				</TableCell>
			</TableRow>
			<TableRow>
				<TableCell
					style={{ paddingBottom: 0, paddingTop: 0 }}
					colSpan={6}
				>
					<Collapse in={open} timeout="auto" unmountOnExit>
						<Box sx={{ margin: 1 }}>
							<Table size="small" aria-label="Hole info">
								<TableHead>
									<TableRow>
										{tracks.map((hole) => (
											<TableCell
												style={{
													maxWidth: "5px",
													padding: "0px 0px",
													textAlign: "center"
													/* borderRight:
														"1px solid black" */
												}}
												key={hole.Number + "holekey"}
											>
												{hole.Number}
											</TableCell>
										))}
									</TableRow>
									<TableRow>
										{tracks.map((hole) => (
											<TableCell
												style={{
													padding: "2px 1px",
													textAlign: "center"
												}}
												key={hole.Number + "parkey"}
											>
												{hole.Par}
											</TableCell>
										))}
									</TableRow>
								</TableHead>
								<TableBody>
									<TableRow>
										{row.PlayerResults.map((score) => (
											<TableCell
												style={{
													padding: "0px 0px",
													textAlign: "center",
													backgroundColor:
														score.Diff < 0
															? "green"
															: "",
													color:
														score.Diff < 0
															? "white"
															: "",
													boxShadow:
														score.Diff > 0
															? "inset 0px 0px 0px 1px red"
															: ""
												}}
												key={score.id + "score"}
											>
												{score.Result}
											</TableCell>
										))}
									</TableRow>
								</TableBody>
							</Table>
						</Box>
					</Collapse>
				</TableCell>
			</TableRow>
		</>
	);
};

export const PlayerList = ({ metrixData }) => {
	const [filterClass, setFilterClass] = useState("");
	const [openFilter, setOpenFilter] = useState(false);
	const anchorRef = useRef(null);
	const [selectedIndex, setSelectedIndex] = useState(0);
	const [options, setOptions] = useState([]);

	const pageChanged = useLocation();

	useEffect(() => {
		setFilterClass("");
		setOptions([]);
	}, [pageChanged]);

	metrixData.Results.filter(
		(value, index, self) =>
			index ===
			self.findIndex(
				(playerClasses) => playerClasses.ClassName === value.ClassName
			)
	).map((playerClass) => {
		if (!options.includes(playerClass.ClassName))
			setOptions((previousState) => [
				...previousState,
				playerClass.ClassName
			]);
	});

	const handleMenuItemClick = (index, option) => {
		setSelectedIndex(index);
		setFilterClass(option);
		setOpenFilter(false);
	};

	const handleToggle = () => {
		setOpenFilter((prevOpen) => !prevOpen);
	};

	const handleClose = (event) => {
		if (anchorRef.current && anchorRef.current.contains(event.target))
			return;

		setOpenFilter(false);
	};

	return (
		<>
			<ButtonGroup
				variant="contained"
				ref={anchorRef}
				aria-label="split button"
			>
				<Button>
					{filterClass === ""
						? "Valitse luokka"
						: options[selectedIndex]}
				</Button>

				<Button
					size="small"
					aria-controls={openFilter ? "split-button-menu" : undefined}
					aria-expanded={openFilter ? "true" : undefined}
					aria-label="select merge strategy"
					aria-haspopup="menu"
					onClick={handleToggle}
				>
					<ArrowDropDownIcon />
				</Button>
			</ButtonGroup>
			<Popper
				open={openFilter}
				anchorEl={anchorRef.current}
				role={undefined}
				transition
				disablePortal
			>
				{({ TransitionProps, placement }) => (
					<Grow
						{...TransitionProps}
						style={{
							transformOrigin:
								placement === "bottom"
									? "center top"
									: "center bottom"
						}}
					>
						<Paper>
							<ClickAwayListener onClickAway={handleClose}>
								<MenuList id="split-button-menu" autoFocusItem>
									{options.map((option, index) => (
										<MenuItem
											key={option}
											disabled={index === 2}
											selected={index === selectedIndex}
											onClick={() =>
												handleMenuItemClick(
													index,
													option
												)
											}
										>
											{option}
										</MenuItem>
									))}
								</MenuList>
							</ClickAwayListener>
						</Paper>
					</Grow>
				)}
			</Popper>

			<TableContainer component={Paper}>
				<Table aria-label="collapsible table">
					<TableHead>
						<TableRow>
							<TableCell align="right">#</TableCell>
							<TableCell>Pelaaja</TableCell>
							<TableCell align="right">+/-</TableCell>
							<TableCell align="right">sum</TableCell>
							<TableCell />
							<TableCell />
						</TableRow>
					</TableHead>
					<TableBody>
						{metrixData.Results.map((row) =>
							row.ClassName === filterClass ? (
								<Row
									key={row.UserID}
									row={row}
									tracks={metrixData.Tracks}
								/>
							) : (
								""
							)
						)}
					</TableBody>
				</Table>
			</TableContainer>
		</>
	);
};
