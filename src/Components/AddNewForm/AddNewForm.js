import React, { useContext, useState } from "react";
import axios from "axios";
import { Button, IconButton, Modal, Stack, TextField } from "@mui/material";
import { Box } from "@mui/system";
import { AccessTime, CalendarMonth } from "@mui/icons-material";
import GetAppIcon from "@mui/icons-material/GetApp";

import { SaveToDb } from "./SaveToDb";
import { UserContext } from "../../UserContext";

export const AddNewForm = () => {
	const [metrixId, setMetrixId] = useState("");
	const [metrixData, setMetrixData] = useState({});
	const [tournamentName, setTournamentName] = useState("");
	const [competitionName, setCompetitionName] = useState("");
	const [date, setDate] = useState("");
	const [time, setTime] = useState("");
	const [enableButtons, setEnableButtons] = useState(true);
	const [errorText, setErrorText] = useState("");
	const { showCompModal, setShowCompModal } = useContext(UserContext);

	const metrixURL = "https://discgolfmetrix.com/";
	const apiURL = metrixURL + "api.php?content=result&id=" + metrixId;

	const HelperText1 =
		"Lorem ipsum on 1500-luvulta lähtien olemassa ollut teksti, jota käytetään usein täytetekstinä ulkoasun testaamiseen graafisessa suunnittelussa, kun oikeata tekstisisältöä ei vielä ole.";
	const HelperText2 =
		"Lorem ipsum on 1500-luvulta lähtien olemassa ollut teksti";

	const wordTest = (string) => {
		const words = string.split("&rarr;");
		const getLast = words.length - 1;

		setTournamentName(words[0].trim());

		const checkLenght =
			words.length > 2
				? words[1].trim() + " >-> " + words[2].trim()
				: words[getLast].trim();
		setCompetitionName(checkLenght);
	};

	const CheckMetrixData = (data) => {
		const idCheck = {}.propertyIsEnumerable.call(data, "Errors")
			? InvalidMetrixId()
			: SaveMetrixData(data);
		return idCheck;
	};

	const SaveMetrixData = (data) => {
		setMetrixData(data);
		const getDate = Date.parse(data.Competition.Date);
		const parseDate = new Date(getDate);
		const dateOptions = {
			weekday: "short",
			year: "numeric",
			month: "numeric",
			day: "numeric"
		};
		setDate(parseDate.toLocaleString(undefined, dateOptions));

		const getTimeString = data.Competition.Time;
		const splitTimeString = getTimeString.split(":", 2);
		const attachHoursMinutes =
			splitTimeString[0] + ":" + splitTimeString[1];

		setTime(attachHoursMinutes);
		wordTest(data.Competition.Name);
		setEnableButtons(false);
	};

	const UpdateMetrixId = (e) => {
		setMetrixId(e.target.value);
		setErrorText("");
	};

	const InvalidMetrixId = () => {
		setErrorText("Virheellinen ID");
	};

	const FetchApiData = async () => {
		axios
			.get(apiURL)
			.then((response) => CheckMetrixData(response.data))
			.catch((error) => console.log(error));
	};

	const CloseFormModal = () => {
		setMetrixId("");
		setMetrixData({});
		setTournamentName("");
		setCompetitionName("");
		setDate("");
		setTime("");
		setShowCompModal(false);
		setErrorText("");
	};

	const SaveForm = () => {
		SaveToDb({
			metrixData,
			metrixId,
			date,
			time,
			tournamentName,
			competitionName
		});
		CloseFormModal();
	};

	return (
		<Modal open={showCompModal} onClose={() => CloseFormModal()}>
			<>
				<Box
					sx={{
						position: "absolute",
						top: "6vh",
						left: "25vw",
						height: "auto",
						width: "50%",
						padding: "2em",
						backgroundColor: "#FBFBFF",
						borderRadius: "1em"
					}}
				>
					<p
						style={{
							fontFamily: "Verdana",
							color: "#616161",
							textShadow: "#e0e0e0 1px 1px 0",
							letterSpacing: "1px",
							wordSpacing: "1px",
							fontWeight: "700",
							textDecoration: "none",
							fontStyle: "normal",
							fontVariant: "small-caps",
							textTransform: "uppercase"
						}}
					>
						Vaihe 1.
					</p>
					<hr />
					<Stack
						direction="row"
						alignItems="baseline"
						justifyContent="center"
						spacing={1}
					>
						<p
							style={{
								fontFamily: "Lucida Sans Unicode",
								letterSpacing: "-1px",
								wordSpacing: "-1px",
								fontWeight: "700",
								textDecoration: "none",
								fontStyle: "normal",
								fontVariant: "small-caps",
								textTransform: "none"
							}}
						>
							{metrixURL}
						</p>
						<TextField
							onChange={UpdateMetrixId}
							required
							id="standard-required"
							label="metrixID"
							helperText={errorText !== "" ? errorText : ""}
							variant="standard"
							sx={{ width: "90px" }}
							inputProps={{
								min: 0,
								style: { textAlign: "center" }
							}}
						/>
						<IconButton
							onClick={FetchApiData}
							disabled={metrixId === ""}
							color="primary"
							variant="outlined"
							aria-label="tallenna"
							sx={{
								display: "inline-flex",
								position: "relative",
								top: "10px"
							}}
						>
							<GetAppIcon />
						</IconButton>
					</Stack>

					<p
						style={{
							fontFamily: "Verdana",
							color: "#616161",
							textShadow: "#e0e0e0 1px 1px 0",
							letterSpacing: "1px",
							wordSpacing: "1px",
							fontWeight: "700",
							textDecoration: "none",
							fontStyle: "normal",
							fontVariant: "small-caps",
							textTransform: "uppercase"
						}}
					>
						Vaihe 2.
					</p>
					<hr />
					<br />
					<Stack
						direction="column"
						alignItems="baseline"
						justifyContent="center"
						spacing={1}
					>
						<TextField
							id="turnauksenNimi"
							label="Turnauksen nimi"
							value={tournamentName}
							helperText={HelperText1}
							disabled={enableButtons}
							fullWidth={true}
						/>
						<br />
						<TextField
							id="osakilpailuNimi"
							label="Osakilpailu nimi"
							value={competitionName}
							helperText={HelperText2}
							disabled={enableButtons}
							fullWidth={true}
							onChange={(e) => setCompetitionName(e.target.value)}
						/>
					</Stack>

					<br />

					<Stack
						direction="row"
						alignContent="center"
						alignItems="center"
						sx={{ float: "left" }}
						spacing={1.5}
					>
						<CalendarMonth
							sx={{ color: "action.active", mr: 1, my: 0.5 }}
						/>

						<TextField
							id="competitionDate"
							hiddenLabel
							value={date}
							disabled={true}
							variant="standard"
							size="small"
							sx={{ width: "120px" }}
							inputProps={{
								min: 0,
								style: { textAlign: "center" }
							}}
						/>

						<AccessTime
							sx={{ color: "action.active", mr: 1, my: 0.5 }}
						/>

						<TextField
							id="competitionTime"
							hiddenLabel
							value={time}
							disabled={true}
							variant="standard"
							size="small"
							sx={{ width: "50px" }}
							inputProps={{
								min: 0,
								style: { textAlign: "center" }
							}}
						/>
					</Stack>

					<Stack
						direction="row"
						spacing={2}
						justifyContent="end"
						sx={{ float: "right" }}
					>
						<Button
							variant="text"
							onClick={() => CloseFormModal()}
							disabled={enableButtons}
						>
							Hylkää
						</Button>
						<Button
							variant="contained"
							onClick={() => SaveForm()}
							disabled={enableButtons}
						>
							Tallenna
						</Button>
					</Stack>
				</Box>
			</>
		</Modal>
	);
};
