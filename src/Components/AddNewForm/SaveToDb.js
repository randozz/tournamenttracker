import axios from "axios";

export const SaveToDb = async ({
	metrixData,
	metrixId,
	date,
	time,
	tournamentName,
	competitionName
}) => {
	const competition = metrixData.Competition;

	const competitionInfo = {
		metrixId: Number(metrixId),
		tournamentName,
		competitionName,
		Type: Number(competition.Type),
		TourDateStart: competition.TourDateStart,
		TourDateEnd: competition.TourDateEnd,
		Date: date,
		Time: time,
		Comment: competition.Comment,
		CourseName: competition.CourseName,
		CourseID: Number(competition.CourseID),
		MetrixMode: Number(competition.MetrixMode),
		ShowPreviousRoundsSum: competition.ShowPreviousRoundsSum,
		HasSubcompetitions: competition.HasSubcompetitions,
		WeeklyHCSummary: competition.WeeklyHCSummary,
		WeeklyHC: competition.WeeklyHC,
		Results: competition.Results,
		Tracks: competition.Tracks
	};

	await axios
		.post("http://localhost:3100/Competition", competitionInfo)
		.then(() => {
			alert(metrixId + " tallennettiin onnistuneesti!");
		})
		.catch((error) => {
			console.log("error", error.response.data);
		});
};
