import { Box, Stack } from "@mui/material";
import React, { useState } from "react";
import { BrowserRouter } from "react-router-dom";
import { AddNewForm } from "./AddNewForm/AddNewForm";

import Router from "./Router/Router";
import { Sidebar } from "./Sidebar/Sidebar";

export const AppContainer = () => {
	const [metrixData, setMetrixData] = useState({});

	const PassMetrixData = (dataPack) => {
		setMetrixData(dataPack);
	};

	return (
		<Box
			sx={{
				backgroundColor: "#F8F8FF",
				padding: "5px",
				width: "97vw"
			}}
		>
			<AddNewForm />
			<BrowserRouter>
				<Stack direction="row" spacing={2}>
					<Sidebar callback={PassMetrixData} />
					<Router data={metrixData} />
				</Stack>
			</BrowserRouter>
		</Box>
	);
};
