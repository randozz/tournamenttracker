import React from "react";
import { Route, Routes } from "react-router-dom";
import { TournamentInfo } from "../Datafields/TournamentInfo";
import { CompetitionInfo } from "../Datafields/CompetitionInfo";

const Router = ({ data }) => (
	<Routes>
		<Route path="/" element={<></>} />
		<Route
			path="/tournament/:id"
			element={<TournamentInfo data={data} />}
		/>

		<Route
			path="/competition/:id"
			element={<CompetitionInfo data={data} />}
		/>
		<Route path="*" element={<h3>Tyhjää täynnä (404)</h3>} />
	</Routes>
);

export default Router;
