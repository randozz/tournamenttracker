import React, { createContext, useState } from "react";

export const UserContext = createContext();

export const UserProvider = ({ children }) => {
	const [showCompModal, setShowCompModal] = useState(false);

	return (
		<UserContext.Provider
			value={{
				showCompModal,
				setShowCompModal
			}}
		>
			{children}
		</UserContext.Provider>
	);
};
