import React from "react";
import { ThemeProvider } from "@mui/material/styles";
import { defaultTheme } from "./Theme/defaultTheme";
import { AppContainer } from "./Components/AppContainer";
import { UserProvider } from "./UserContext";

const App = () => (
	<ThemeProvider theme={defaultTheme}>
		<UserProvider>
			<AppContainer />
		</UserProvider>
	</ThemeProvider>
);

export default App;
